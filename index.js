const cron = require('node-cron')
const dotenv = require('dotenv').config({path: __dirname + '/.env'})
const Nightmare = require('nightmare')
let nightmare = Nightmare({ show: true, waitTimeout: process.env.WAIT_TIMEOUT * 1000 })

const url = 'https://backoffice.api-ux.com/en_US/web/login'
const email = process.env.EMAIL
const pass = process.env.PW
const dailyHours = JSON.parse(process.env.DAILY_HOURS)
const iterations = process.env.ITERATIONS
const message = process.env.MESSAGE
let hours = dailyHours[new Date().getDay()-1]

if (process.env.SCHEDULE === 'true') {
  cron.schedule('0 17 * * *', function(){
    hours = dailyHours[new Date().getDay()-1]
    start()
  });
} else {
  start()
}


function start () {
  nightmare
    .goto(url)
    .insert('#login', email)
    .insert('#password', pass)
    .click('button.btn.btn-primary')
    .wait('#oe_main_menu_navbar')
    .click('ul.oe_application_menu_placeholder > li > a[data-menu="191"]')
    .wait('div[data-menu-parent="191"]')
    .click('a[data-menu="313"]')
    .wait('body > div.openerp.openerp_webclient_container > table > tbody > tr > td.oe_application > div > div > table > tbody > tr:nth-child(2) > td > div > div > div.oe_searchview_facets > div:nth-child(4) > span.oe_facet_remove')
    .click('body > div.openerp.openerp_webclient_container > table > tbody > tr > td.oe_application > div > div > table > tbody > tr:nth-child(2) > td > div > div > div.oe_searchview_facets > div:nth-child(4) > span.oe_facet_remove')
    .wait('table.oe_list_content > tbody > tr')

  for ( let i = 0; i < iterations; i++) {
    nightmare = addDay(nightmare)
  }

  nightmare
    .end()
    .catch(error => {
      console.error('Search failed:', error)
    })
}

function addDay (nightmare) {
  return nightmare
    .click('button.oe_list_add')
    .wait('div.oe_editing')
    .wait('span[data-fieldname="name"] > input')
    .insert('span[data-fieldname="name"] > input', message)
    .insert('span[data-fieldname="account_id"] > div > input', '1200')
    .click('span[data-fieldname="account_id"] > div > span.oe_m2o_drop_down_button')
    .wait('#ui-id-2 > li')
    .click('#ui-id-2 > li')
    .click('span[data-fieldname="task_id"] > div > span.oe_m2o_drop_down_button')
    .wait('#ui-id-3 > li:last-child')
    .click('#ui-id-3 > li:last-child')
    .evaluate(function() {
      document.querySelector('span[data-fieldname="unit_amount"] > input').value = ''
    })
    .insert('span[data-fieldname="unit_amount"] > input', `0${hours}:00`)
    .click('button.oe_list_save')
    .wait('div[class="oe_list_buttons"]')
}