# Odoo Assistance

## Instalación del proyecto
Primero instalamos las dependencias del proyecto
```
git clone git clone https://dvilendez@bitbucket.org/dvilendez/odoo_assistance.git
cd odoo_assistance
npm install
```
Creamos nuestro archivo de configuracion llamado **.env** y lo dejamos en la raíz del proyecto
```
# Email apiux
EMAIL=tu-email@correo.com
# Clave
PW=tu_contraseña
# Ejecución programada
SCHEDULE=false
# Horas de la semana. Ej: Lunes=>8, Martes =>8, ..., Viernes=>7
DAILY_HOURS=[8,8,8,8,7]
# Segundos para esperar hasta que aparezca el elemento
WAIT_TIMEOUT=120
# El mensaje de la firma
MESSAGE=Cambia este mensaje
# La cantidad de firmas que hará
ITERATIONS=1
```
Instalaremos PM2 para automatizar el inicio del script de firma automatica
```
sudo npm install pm2 -g
```
Luego necestiremos ejecutar el siguiente comando para que PM2 nos indique un comando para inicializar PM2 cada vez que nuestro sistema se inicie
```
pm2 startup upstart 
```
Ejecutamos el comando indicado por el comando anterior :
```
[PM2] To setup the Startup Script, copy/paste the following command:
sudo env PATH=$PATH:/home/diego/.nvm/versions/node/v12.6.0/bin /usr/local/lib/node_modules/pm2/bin/pm2 startup upstart -u diego --hp /home/diego
```
Iniciamos nuestro script con PM2 de la siguiente forma:
```
pm2 start index.js --name odoo_assistance_cron
```
Luego necesitaremos guardar nuestra lista de procesos que se encuentran agregados en PM2.
Puedes ver tu lista de procesos agregados en PM2 con el siguiente comando:
```
pm2 list
```
Si aparece nuestro proceso llamado **odoo_assistance_cron** entonces guardamos con:
```
pm2 save
```
Y listo! Puedes reiniciar tu computador y revisar si aparece el script inicializado.